import tkinter as tk
from tkinter import ttk
from ttkthemes import ThemedTk



class myApp(ThemedTk):
    def __init__(self, theme="arc"):
        ThemedTk.__init__(self, fonts=True, themebg=True)
        self.set_theme(theme)

        # GUI creation
        self.title("New ttk project")

        bonjour_label = ttk.Label(self, text="Bonjour.")
        self.print_button = ttk.Button(
            self,
            text="Print something",
            command=self.printSomething)
        
        bonjour_label.grid(column=1, row=1, padx=20, pady=20)
        self.print_button.grid(column=2, row=2, padx=20, pady=20)

        self.fix_gui_size()

    
    def fix_gui_size(self):
        self.update()
        self.minsize(self.winfo_width(), self.winfo_height())
        self.maxsize(self.winfo_width(), self.winfo_height())
    
    def setEntry(self, entry, text):
        entry.delete(0, tk.END)
        entry.insert(0, text)
    
    
    def printSomething(self):
        print("Something")



if __name__ == "__main__":
    self = myApp()
    self.mainloop()